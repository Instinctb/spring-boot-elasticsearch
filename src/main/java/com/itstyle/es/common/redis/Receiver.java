package com.itstyle.es.common.redis;

import cn.hutool.json.JSONUtil;
import com.itstyle.es.entity.SysLogs;
import com.itstyle.es.repository.ElasticLogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class Receiver {
    @Resource
    private ElasticLogRepository<SysLogs, Long> elasticLogRepository;

    private CountDownLatch latch;

    @Autowired
    public Receiver(CountDownLatch latch) {
        this.latch = latch;
    }

    public void receiveMessage(String message) {
        log.info("接收log消息 <{}>", message);
        if (message == null) {
            log.info("接收log消息 <" + null + ">");
        } else {
            try {
                SysLogs sysLogs = JSONUtil.toBean(message, SysLogs.class);
                elasticLogRepository.save(sysLogs);
                log.info("接收log消息内容-保存 ----成功---");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        latch.countDown();
    }
}
