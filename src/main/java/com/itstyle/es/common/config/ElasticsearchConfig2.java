// package com.itstyle.es.common.config;
//
// import org.apache.http.HttpHost;
// import org.apache.http.auth.AuthScope;
// import org.apache.http.auth.UsernamePasswordCredentials;
// import org.apache.http.client.CredentialsProvider;
// import org.apache.http.impl.client.BasicCredentialsProvider;
// import org.elasticsearch.client.RestClient;
// import org.elasticsearch.client.RestClientBuilder;
// import org.elasticsearch.client.RestHighLevelClient;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
//
// /**
//  * @description:
//  * @author: bo
//  * @create: 2024-11-26 16:06
//  */
// @Configuration
// public class ElasticsearchConfig2 {
//
//     @Value("${spring.elasticsearch.rest.username:elastic}")
//     private String username;
//
//     @Value("${spring.elasticsearch.rest.password:123456}")
//     private String password;
//
//     @Value("${elasticsearch.clusterNodes}")
//     private String clusterNodes;
//
//     // @Bean
//     // public RestHighLevelClient restHighLevelClient() {
//     //     CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
//     //     credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));
//     //
//     //     RestClientBuilder builder = RestClient.builder(HttpHost.create(clusterNodes))
//     //             .setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
//     //
//     //     return new RestHighLevelClient(builder);
//     // }
//
//     @Bean(destroyMethod = "close")
//     public RestHighLevelClient getRestHighLevelClient() {
//         RestClientBuilder builder = RestClient.builder(
//                 new HttpHost(clusterNodes, 9300));
//         // 异步httpclient连接延时配置
//         builder.setRequestConfigCallback(requestConfigBuilder -> {
//             requestConfigBuilder.setConnectTimeout(30 * 60);
//             requestConfigBuilder.setSocketTimeout(30 * 60);
//             requestConfigBuilder.setConnectionRequestTimeout(30 * 60);
//             return requestConfigBuilder;
//         });
//
//         // 用户认证对象
//         CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
//         // 设置账号密码
//         credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));
//
//         // 异步httpclient连接数配置
//         builder.setHttpClientConfigCallback(httpClientBuilder -> {
//             httpClientBuilder.setMaxConnTotal(100);
//             httpClientBuilder.setMaxConnPerRoute(100);
//
//             // 设置账号密码
//             httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
//             return httpClientBuilder;
//         });
//
//         builder.setHttpClientConfigCallback(httpAsyncClientBuilder ->
//                 httpAsyncClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
//
//         return new RestHighLevelClient(builder);
//     }
// }