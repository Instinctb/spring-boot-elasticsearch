package com.itstyle.es.entity;

import cn.hutool.core.util.RandomUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.zxp.esclientrhl.annotation.ESID;
import org.zxp.esclientrhl.annotation.ESMapping;
import org.zxp.esclientrhl.annotation.ESMetaData;
import org.zxp.esclientrhl.enums.DataType;
import org.zxp.esclientrhl.repository.GeoEntity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @Document 参数说明
 * String indexName();//索引库的名称，个人建议以项目的名称命名
 * <p>
 * String type() default "";//类型，个人建议以实体的名称命名
 * <p>
 * short shards() default 5;//默认分区数
 * <p>
 * short replicas() default 1;//每个分区默认的备份数
 * <p>
 * String refreshInterval() default "1s";//刷新间隔
 * <p>
 * String indexStoreType() default "fs";//索引文件存储类型
 */
//@Document 代表在定义ES中的文档document
// indexName 索引名称，一般为全小写字母，可以看成是数据库名称
// type 类型，可以看成是数据库表名
// useServerConfiguration 是否使用系统配置
// shards 集群模式下分片存储，默认分5片
// replicas 数据复制几份，默认1份
// refreshInterval 多久刷新数据，默认1s
// indexStoreType 索引存储模式，默认FS
// createIndex 是否创建索引，默认True,代表不存在indexName对应索引时，自动创建
// @Document(indexName="elasticsearch",type="sysLog",indexStoreType="fs",shards=5,replicas=1,refreshInterval="-1")




//    /**
//  * 检索时的索引名称，如果不配置则默认为和indexName一致，该注解项仅支持搜索
//  * 并不建议这么做，建议通过特定方法来做跨索引查询
//  */
// String[] searchIndexNames() default {};
// /**
//  * 索引名称，必须配置
//  */
// String indexName();
// /**
//  * 索引类型，可以不配置，不配置默认为_doc，墙裂建议每个index下只有一个type
//  */
// String indexType() default "";
// /**
//  * 主分片数量
//  */
// int number_of_shards() default 5;
// /**
//  * 备份分片数量
//  */
// int number_of_replicas() default 1;
// /**
//  * 是否打印日志
//  * @return
//  */
// boolean printLog() default false;
// /**
//  * 别名、如果配置了后续增删改查都基于这个alias
//  * 当配置了此项后自动创建索引功能将失效
//  * indexName为aliasName
//  * @return
//  */
//  boolean alias() default false;
//
// /**
// * 别名对应的索引名称
// * 当前配置仅生效于配置了alias但没有配置rollover
// * 注意：所有配置的index必须存在
// * @return
// */
// String[] aliasIndex() default {};
//
// /**
// * 当配置了alias后，指定哪个index为writeIndex
// * 当前配置仅生效于配置了alias但没有配置rollover
// * 注意：配置的index必须存在切在aliasIndex中
// * @return
// */
// String writeIndex() default "";
//
// /**
// * 当配置了rollover为true时，开启rollover功能（并忽略其他alias的配置）
// * aliasName为indexName
// * 索引名字规格为：indexName-yyyy.mm.dd-00000n
// * 索引滚动生成策略如下
// * @return
// */
// boolean rollover() default false;
//
//  /**
//  * 自动执行rollover相关配置
//  * 自动执行rollover开关
//  * @return
//  */
//  boolean autoRollover() default false;
//
// /**
// * 自动执行rollover相关配置
// * 项目启动后延迟autoRolloverInitialDelay时间后开始执行
// * @return
// */
// long autoRolloverInitialDelay() default 0L;
//
// /**
// * 自动执行rollover相关配置
// * 项目启动后每间隔autoRolloverPeriod执行一次
// * @return
// */
// long autoRolloverPeriod() default 4L;
//
// /**
// * 自动执行rollover相关配置
// * 单位时间配置，与autoRolloverPeriod、autoRolloverInitialDelay对应
// * @return
// */
// TimeUnit  autoRolloverTimeUnit() default TimeUnit.HOURS;
//
//
// /**
// * 当前索引超过此项配置的时间后生成新的索引
// * @return
// */
// long rolloverMaxIndexAgeCondition() default 0L;
//
// /**
// * 与rolloverMaxIndexAgeCondition联合使用，对应rolloverMaxIndexAgeCondition的单位
// * @return
// */
// TimeUnit rolloverMaxIndexAgeTimeUnit() default TimeUnit.DAYS;
//
// /**
// * 当前索引文档数量超过此项配置的数字后生成新的索引
// * @return
// */
// long rolloverMaxIndexDocsCondition() default 0L;
//
// /**
// * 当前索引大小超过此项配置的数字后生成新的索引
// * @return
// */
// long rolloverMaxIndexSizeCondition() default 0L;
//
// /**
// * 与rolloverMaxIndexSizeCondition联合使用，对应rolloverMaxIndexSizeCondition的单位
// * @return
// */
// ByteSizeUnit rolloverMaxIndexSizeByteSizeUnit() default ByteSizeUnit.GB;
//
// /**
// * 最大分页深度
// * @return
// */
// long maxResultWindow() default 10000L;
//
// /**
// * 索引名称是否自动包含后缀
// * @return
// */
// boolean suffix() default false;
//
// /**
// * 是否自动创建索引
// * @return
// */
// boolean autoCreateIndex() default true;
@ESMetaData(indexName="sys_log",number_of_shards = 5,number_of_replicas = 1,printLog = true)
@Data
public class SysLogs implements Serializable{


    //@Field 文档中的字段类型，对应的是ES中document的Mappings概念，是在设置字段类型
    // type 字段类型，默认按照java类型进行推断，也可以手动指定，通过FieldType枚举
    // index 是否为每个字段创建倒排索引,默认true,如果不想通过某个field的关键字来查询到文档,设置为false即可
    // pattern 用在日期上类型字段上 format = DateFormat.custom, pattern = “yyyy-MM-dd HH:mm:ss”
    // searchAnalyzer 指定搜索的分词，ik分词只有ik_smart(粗粒度)和ik_max_word(细粒度)两个模式，具体差异大家可以去ik官网查看
    // analyzer 指定索引时的分词器，ik分词器有ik_smart和ik_max_word
    // store 是否存储到文档的_sourch字段中，默认false情况下不存储


    /**
     *
     * @Field 参数说明
     * FieldType type() default FieldType.Auto;#自动检测属性的类型

     * FieldIndex index() default FieldIndex.analyzed;#默认情况下分词

     * DateFormat format() default DateFormat.none;

     * String pattern() default "";

     * boolean store() default false;#默认情况下不存储原文

     * String searchAnalyzer() default "";#指定字段搜索时使用的分词器

     * String indexAnalyzer() default "";#指定字段建立索引时指定的分词器

     * String[] ignoreFields() default {};#如果某个字段需要被忽略

     * boolean includeInParent() default false;
     */
    /**
     * 日志id
     */
    @ESID
    private Long id;

    /**
     * 操作用户id
     */
    private Long userId;

    /**
     * 操作用户
     */
    // @Field(analyzer="ik_max_word",searchAnalyzer="ik_max_word")

    // 分词器 @ESMapping(datatype = DataType.text_type,custom_analyzer = "pinyin_analyzer")
    private String username;

    /**
     * 操作
     */
    private String operation;

    /**
     * 方法
     */
    private String method;

    /**
     * 参数
     */
    private String params;

    /**
     * 耗时
     */
    private Long time;

    /**
     * 操作ip地址
     */
    private String ip;

    /**
     * 创建时间
     */
    private Timestamp gmtCreate;
    /**
     * 设备类型
     */
    private Short deviceType;
    /**
     * 日志类型
     */
    private Short logType;
    /**
     * 异常信息
     */
    private String exceptionDetail;
    /**
     * 平台
     */
    private Short platFrom;

    // @Field(type = FieldType.Date, format = DateFormat.custom, pattern ="yyyy-MM-dd HH:mm:ss")// 指定存储格式
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    // 数据格式转换，并加上8小时进行存储
    private String uploadTime;//这里类型是String，进入es就转换为Date类型

    // @ESMapping(datatype = DataType.geo_point_type)
    // GeoEntity geo;


    // public SysLogs() {
    //     this.id = RandomUtil.randomLong();
    //     this.userId = 12L;
    //     this.username = "张三";
    //     this.operation = "张三";
    //     this.method = "方法";
    //     this.params = "参数";
    //     this.time = 2L;
    //     this.ip = "192.168.1.190";
    //     this.gmtCreate = new Timestamp(new Date().getTime());
    //     this.deviceType = 1;
    //     this.logType = 1;
    //     this.exceptionDetail = "详细";
    //     this.platFrom = 1;
    // }

}
