package com.itstyle.es;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.zxp.esclientrhl.annotation.EnableESTools;


//ES依赖一个重要的组件：Lucene，关于数据结构的优化通常来说是对Lucene的优化，它是集群的一个存储于检索工作单元
@SpringBootApplication
@Slf4j
@EnableESTools
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        log.info("全文搜索服务启动");
    }
}